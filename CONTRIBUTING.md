# Contributing to Gradebook

:+1::tada: First off, thanks for taking the time to contribute! :tada::+1:

For right now (as this is a very small project), simply make a pull request in another branch and make a request. All that I ask is that you follow these rules.

- Simple commit messages, one commit per major change.

## Things that Need to be done

- Finish this document. Please.