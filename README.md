# Gradebook

Gradebook is a cloud-enabled, mobile-based grade management app.

## Features

  - Sync directly with the Aeries gradebook. Let your teachers manage your grades, not you!
  - Beautiful material design, matches the spec as much as possible in order to match with the rest of the UI.

## Planned

  - Support for other student information systems.
  - Fingerprint reader for security, or PIN if your device doesn't support it.
  - Offline gradebook, use the latest data from Aeries to populate your gradebook.
  - Imagine: see what your grade would be if you had just gotten one more question right!

## Feature Requests

Have a great idea for a feature that would be helpful to a lot of people at once? Want me to add another SIS? Just create an issue on the github page, and if it's worth I'll move it to my private GitLab.

## Support

Unfortunately I'm not able to provide support (I'm just one person, and I have schoolwork to do) - so if you need help with something then ask a friend. If the support you need is because of a bug, post as much device info as you can as well as what issue you're having in the GitHub bugs page.

## Donate

If you enjoy the app I'd appreciate a donation at one of these addresses:

[PayPal](https://www.paypal.me/NikhilJha) / [TipMe](http://nikhilj.tip.me) / Bitcoin @  1GradevQEuk1BrN7Qhwso1bz79SVBKVs7E

Want to donate with an altcoin? Just send the altcoin to Changelly/Shapeshift and input the BTC address as your conversion address.
