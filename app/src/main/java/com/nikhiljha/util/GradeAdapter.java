package com.nikhiljha.util;

        import android.support.v7.widget.RecyclerView;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;

        import com.nikhiljha.grades.R;

        import java.util.List;

public class GradeAdapter extends RecyclerView.Adapter<GradeAdapter.ContactViewHolder> {

    private List<Aeries.GradebookSummaryData.Result> contactList;

    public GradeAdapter(List<Aeries.GradebookSummaryData.Result> contactList) {
        this.contactList = contactList;
    }


    @Override
    public int getItemCount() {
        return contactList.size();
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        Aeries.GradebookSummaryData.Result ci = contactList.get(i);
        contactViewHolder.vName.setText(ci.className.split("(?<=\\G.{25})")[0]);
        contactViewHolder.vGrade.setText(ci.percentGrade + "%");
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.card_item, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {

        protected TextView vName;
        protected TextView vGrade;

        public ContactViewHolder(View v) {
            super(v);
            vName =  (TextView) v.findViewById(R.id.class_name);
            vGrade = (TextView)  v.findViewById(R.id.class_percent);
        }
    }
}