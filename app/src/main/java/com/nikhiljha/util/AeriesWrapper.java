package com.nikhiljha.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.nikhiljha.util.AddCookiesInterceptor;
import com.nikhiljha.util.Aeries;
import com.nikhiljha.util.ReceivedCookiesInterceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Nikhil Jha on 7/28/2016.
 * License: MIT
 */
public class AeriesWrapper {

    // Need to check every time something is called to make sure user is still logged in.
    // Now in order to remedy that I can just call this class and give it context!
    // Everything else is done through SharedPreferences Cookies.
    public boolean refreshLogin(Context context) {
        SharedPreferences settings = context.getSharedPreferences("GradebookDB", 0);
        String mEmail = settings.getString("Email", null);
        String mPassword = settings.getString("Password", null);

        OkHttpClient client = new OkHttpClient();
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new AddCookiesInterceptor(context));
        builder.addInterceptor(new ReceivedCookiesInterceptor(context));
        client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://my.iusd.org")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our GitHub API interface.
        Aeries.API aeries = retrofit.create(Aeries.API.class);

        Call<Aeries.LoginParentStudent.root> call = aeries.LOGIN_PARENT_STUDENT_CALL(new Aeries.LoginObject(mEmail, mPassword, 0));
        Aeries.LoginParentStudent.root rootobj = null;
        try {
            rootobj = call.execute().body();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        if (rootobj.d.Key.equals("Success")) {
            SharedPreferences.Editor editor = settings.edit();
            editor.putString("Email", mEmail);
            editor.putString("Password", mPassword);
            editor.apply();
            editor.commit();
            return true;
        } else {
            return false;
        }
    }

    // Need to return the list of users sometimes.
    // That takes a lot of typing, so I'll just move it here so my code is readable.
    public void getStudents(Context context, Callback callback) {
        OkHttpClient client = new OkHttpClient();
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.addInterceptor(new AddCookiesInterceptor(context));
        builder.addInterceptor(new ReceivedCookiesInterceptor(context));
        builder.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Proxy-Connection", "keep-alive")
                        .header("Connection", "keep-alive")
                        .header("Accept", "*/*")
                        .header("Accept-Language", "en-US;q=1")
                        .header("Content-Type", "application/json")
                        .header("User-Agent", "Gradebook/0.1.0 (Android)");

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        client = builder.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://my.iusd.org")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Aeries.API aeries = retrofit.create(Aeries.API.class);

        Call<Aeries.GetStudents.RootObject> call = aeries.GET_STUDENTS_OF_CURRENT_ACCOUNT();
        call.enqueue(callback);
    }

}
