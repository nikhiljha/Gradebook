package com.nikhiljha.util;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Nikhil Jha on 7/26/2016.
 * License: MIT
 */

public class Aeries {
    public static class LoginObject {
        public final String username;
        public final String password;
        public final int language;

        public LoginObject(String username, String password, int language) {
            this.username = username;
            this.password = password;
            this.language = language;
        }
    }

    public class LoginParentStudent {
        public class D {
            public final String Key;
            public final String Value;

            public D(String key, String value) {
                this.Key = key;
                this.Value = value;
            }
        }
        public class root {
            public final D d;
            public root(D d) {
                this.d = d;
            }
        }
    }

    public class GetStudents {
        public class Result
        {
            public String nameOfStudent;
            public String nameOfSchool;
            public int grade;
            public boolean isStudentActive;
            public boolean isCurrentStudent;
            public int schoolCode;
            public int studentNumber;

            public Result(String nameOfStudent, String nameOfSchool, int grade, boolean isCurrentStudent, boolean isStudentActive, int schoolCode, int studentNumber) {
                this.nameOfStudent = nameOfStudent;
                this.nameOfSchool = nameOfSchool;
                this.grade = grade;
                this.isStudentActive = isStudentActive;
                this.isCurrentStudent = isCurrentStudent;
                this.schoolCode = schoolCode;
                this.studentNumber = studentNumber;
            }
        }

        public class D
        {
            public List<Result> results;

            public D(List<Result> results){
                this.results = results;
            }
        }

        public class RootObject
        {
            public D d;

            public RootObject(D d) {
                this.d = d;
            }
        }
    }

    public class ChangeToStudent {
        public class Results {
            public boolean isStudentChanged;
            public String messageStatus;

            public Results(boolean isStudentChanged, String messageStatus) {
                this.isStudentChanged = isStudentChanged;
                this.messageStatus = messageStatus;
            }
        }

        public class D {
            public Results results;
            public D(Results results) {
                this.results = results;
            }
        }

        public class RootObject {
            public D d;
            public RootObject(D d) {
                this.d = d;
            }
        }
    }

    public static class ChangeToSend {
        public String schoolCode;
        public String studentNumber;
        public ChangeToSend(String schoolCode, String studentNumber) {
            this.schoolCode = schoolCode;
            this.studentNumber = studentNumber;
        }
    }

    public class GetDistricts {
        public class Result
        {
            public String __type;
            public String aeriesURLString;
            public String className;
            public String createdAt;
            public boolean disabled;
            public String disabledReason;
            public String name;
            public String objectId;
            public String updatedAt;

            public Result(String __type, String aeriesURLString, String className, String createdAt, boolean disabled, String disabledReason, String name, String objectId, String updatedAt) {
                this.__type = __type;
                this.aeriesURLString = aeriesURLString;
                this.className = className;
                this.createdAt = createdAt;
                this.disabled = disabled;
                this.disabledReason = disabledReason;
                this.name = name;
                this.objectId = objectId;
                this.updatedAt = updatedAt;
            }
        }

        public class RootObject
        {
            public List<Result> result;

            public RootObject(List<Result> result) {
                this.result = result;
            }
        }
    }

    public class GradebookSummaryData {
        public class Result
        {
            public String gradebookNumberTerm;
            public int gradebookNumber;
            public String term;
            public String code;
            public int period;
            public String mark;
            public String className;
            public int missingAssignments;
            public String updated;
            public String trendDirection;
            public int percentGrade;
            public String comment;
            public boolean isUsingCheckMarks;
            public boolean hideOverallScore;
            public boolean doingRubric;

            public Result(String gradebookNumberTerm, int gradebookNumber, String term, String code, int period, String mark, String className, int missingAssignments, String updated, String trendDirection, int percentGrade, String comment, boolean isUsingCheckMarks, boolean hideOverallScore, boolean doingRubric) {
                this.gradebookNumberTerm = gradebookNumberTerm;
                this.gradebookNumber = gradebookNumber;
                this.term = term;
                this.code = code;
                this.period = period;
                this.mark = mark;
                this.className = className;
                this.missingAssignments = missingAssignments;
                this.updated = updated;
                this.trendDirection = trendDirection;
                this.percentGrade = percentGrade;
                this.comment = comment;
                this.isUsingCheckMarks = isUsingCheckMarks;
                this.hideOverallScore = hideOverallScore;
                this.doingRubric = doingRubric;
            }
        }

        public class D
        {
            public List<Result> results;

            public D(List<Result> results) {
                this.results = results;
            }
        }

        public class RootObject
        {
            public D d;

            public RootObject(D d) {
                this.d = d;
            }
        }
    }

    public interface API {
        @POST("/m/api/MobileWebAPI.asmx/LoginParentStudent")
        Call<LoginParentStudent.root> LOGIN_PARENT_STUDENT_CALL(@Body LoginObject body);

        @GET("/m/api/MobileWebAPI.asmx/GetStudentsOfCurrentAccount")
        Call<GetStudents.RootObject> GET_STUDENTS_OF_CURRENT_ACCOUNT();

        @POST("/m/api/MobileWebAPI.asmx/ChangeToStudent")
        Call<ChangeToStudent.RootObject> CHANGE_TO_STUDENT_CALL(@Body ChangeToSend body);

        @GET("/m/api/MobileWebAPI.asmx/GetGradebookSummaryData")
        Call<GradebookSummaryData.RootObject> GET_SUMMARY_DATA();

        @GET("/bins/4fo13")
        Call<GetDistricts.RootObject> GET_DISTRICT_CALL();
    }


}
