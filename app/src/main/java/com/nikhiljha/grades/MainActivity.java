package com.nikhiljha.grades;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.nikhiljha.util.AeriesWrapper;

public class MainActivity extends AppCompatActivity {

    String TAG = "Grades";

     /*
     The purpose of this activity is to detect whether or not the user is logged in.
     If the user is logged in then it needs to refresh cookies and go back to the app.
     If the user is not logged in then it needs to send the user to the login screen.
      */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // If the user manages to close the app while this is running then we'll need to go back.
        super.onCreate(savedInstanceState);

        // Load up the shared preferences DB so we can store and recieve values.
        SharedPreferences settings = getSharedPreferences("GradebookDB", 0);

        // If the app has never been launched before it won't have this flag, so we should double check.
        if (settings.contains("isLoggedIn")) {
            if (settings.getBoolean("isLoggedIn", true)) {
                // Refresh login just in case.
                UserLoginTask mAuthTask = new UserLoginTask(getApplicationContext());
                mAuthTask.execute((Void) null);

                // Launch the main screen.
                Intent intent = new Intent(this, GradebookActivity.class);
                startActivity(intent);
                finish();
            } else {
                // Send them to the login screen!
                Intent intent = new Intent(this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        } else {
            // App has never been launched before, or was killed before this ran.
            // Let's write the default settings to DB.
            SharedPreferences.Editor editor = settings.edit();
            editor.putBoolean("isLoggedIn", false);
            editor.apply();
            editor.commit();

            // Send them to the login screen!
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    // I hate that this has to be here, but whatever I guess.
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private Context context;

        UserLoginTask(Context context) {
            this.context = context;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            AeriesWrapper aeries = new AeriesWrapper();
            return aeries.refreshLogin(context);
        }

        @Override
        protected void onPostExecute(final Boolean success) {

        }

        @Override
        protected void onCancelled() {

        }
    }
}
