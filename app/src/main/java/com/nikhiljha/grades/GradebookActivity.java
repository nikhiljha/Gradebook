package com.nikhiljha.grades;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.nikhiljha.util.AddCookiesInterceptor;
import com.nikhiljha.util.Aeries;
import com.nikhiljha.util.AeriesWrapper;
import com.nikhiljha.util.GradeAdapter;
import com.nikhiljha.util.ReceivedCookiesInterceptor;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GradebookActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    String TAG = "Grades";

    /*
    This is one of the worst classes I've ever written.
    Callbacks, callbacks everywhere!
     */

    /*
    Callback Definitions:
    getStudents: This gets the students and runs the student selector.
    createGradeCards: This is the return getStudents for the get gradebook summary. It makes the card objects.
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gradebook);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Log.v(TAG, "GradebookActivity: Beginning networking code...");

        AeriesWrapper wrapper = new AeriesWrapper();
        wrapper.getStudents(getApplicationContext(), getStudents);

    }

    public Callback createGradeCards = new Callback<Aeries.GradebookSummaryData.RootObject>() {

        @Override
        public void onResponse(Call<Aeries.GradebookSummaryData.RootObject> call, Response<Aeries.GradebookSummaryData.RootObject> response) {

            RecyclerView recList = (RecyclerView) findViewById(R.id.grade_list_view);
            recList.setHasFixedSize(true);
            LinearLayoutManager llm = new LinearLayoutManager(getApplicationContext());
            llm.setOrientation(LinearLayoutManager.VERTICAL);
            recList.setLayoutManager(llm);

            GradeAdapter ca = new GradeAdapter(response.body().d.results);
            recList.setAdapter(ca);
        }

        @Override
        public void onFailure(Call<Aeries.GradebookSummaryData.RootObject> call, Throwable t) {
            Log.v(TAG, "There was some kind of an error relating to getting gradebook summary data.", t);
        }
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.gradebook, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public Callback getStudents = new Callback<Aeries.GetStudents.RootObject>() {
        @Override
        public void onResponse(Call<Aeries.GetStudents.RootObject> call, final Response<Aeries.GetStudents.RootObject> response) {
            TextView view = (TextView)findViewById(R.id.nav_name);
            if (response.body().d != null) {

                view.setText(response.body().d.results.get(0).nameOfStudent);

                // Load grades.
                OkHttpClient client = new OkHttpClient();
                OkHttpClient.Builder builder = new OkHttpClient.Builder();

                builder.addInterceptor(new AddCookiesInterceptor(getApplicationContext()));
                client = builder.build();

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl("https://my.iusd.org:443")
                        .client(client)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                // Create an instance of our GitHub API interface.
                Aeries.API aeries = retrofit.create(Aeries.API.class);

                Call<Aeries.ChangeToStudent.RootObject> dcall = aeries.CHANGE_TO_STUDENT_CALL(new Aeries.ChangeToSend(Integer.toString(response.body().d.results.get(0).studentNumber), Integer.toString(response.body().d.results.get(0).schoolCode)));
                Log.v(TAG, "Enqueueing changetostudent code.");
                dcall.enqueue(changeToStudentBack);
            } else {
                view.setText("Student");
                LoadClasses();
            }
        }

        @Override
        public void onFailure(Call<Aeries.GetStudents.RootObject> call, Throwable t) {

        }
    };

    public void LoadClasses() {
        OkHttpClient newc = new OkHttpClient();
        OkHttpClient.Builder newb = new OkHttpClient.Builder();

        newb.addInterceptor(new AddCookiesInterceptor(getApplicationContext()));
        newb.addInterceptor(new ReceivedCookiesInterceptor(getApplicationContext()));
        newb.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Proxy-Connection", "keep-alive")
                        .header("Connection", "keep-alive")
                        .header("Accept", "*/*")
                        .header("Accept-Language", "en-US;q=1")
                        .header("Content-Type", "application/json")
                        .header("User-Agent", "Gradebook/0.1.0 (Android)");

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        newc = newb.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://my.iusd.org:443")
                .client(newc)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        // Create an instance of our GitHub API interface.
        Aeries.API aeries = retrofit.create(Aeries.API.class);

        Call<Aeries.GradebookSummaryData.RootObject> ringring = aeries.GET_SUMMARY_DATA();
        Log.v(TAG, "enqueueing gradeSummary data - createGradeCards");
        ringring.enqueue(createGradeCards);
    }

    public Callback changeToStudentBack = new Callback<Aeries.ChangeToStudent>() {
        @Override
        public void onResponse(Call<Aeries.ChangeToStudent> call, Response<Aeries.ChangeToStudent> response) {
            Log.v(TAG, "Recieved response from changetostudent... trying getParentStewdent data");
            LoadClasses();
        }

        @Override
        public void onFailure(Call<Aeries.ChangeToStudent> call, Throwable t) {

        }
    };

    public void onPress(View view) {
        AeriesWrapper wrapper = new AeriesWrapper();
        wrapper.getStudents(getApplicationContext(), getStudents);
    }
}
